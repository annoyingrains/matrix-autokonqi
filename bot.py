"""
SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
SPDX-FileCopyrightText: 2023 Kye Potter annoyingrain5@gmail.com
"""

import simplematrixbotlib as botlib
from nio import RoomMessageText, MatrixRoom
import json
import os

# dotenv is an optional dependency
try:
    import dotenv
except ImportError:
    print("python-dotenv not found, .env files will not be read!")
else:
    dotenv.load_dotenv()

creds = botlib.Creds(
    os.getenv("AUTOKONQI_HOMESERVER"),
    os.getenv("AUTOKONQI_USERNAME"),
    os.getenv("AUTOKONQI_PASSWORD"),
)
bot = botlib.Bot(creds)
PREFIX = "!"

with open("phrases.json") as f:
    phrases = json.load(f)["phrases"]


def MatchFromPhrases(message: str, phrases: list) -> dict | None:
    for phrase in phrases:
        if (
            phrase["phrase"] in message.lower()
        ):  # check the actual string itself, not the dict
            return phrase  # return the first one found


@bot.listener.on_message_event
async def echo(room: MatrixRoom, message: RoomMessageText):
    match = botlib.MessageMatch(room, message, bot)

    if match.is_not_from_this_bot():
        phrasematch = MatchFromPhrases(message.body, phrases)
        if phrasematch is not None:
            await bot.api.send_markdown_message(
                os.getenv("AUTOKONQI_ADMIN_ROOM"),
                f"Alert! Potential misuse! [Link to message in #{room.name}](https://matrix.to/#/{room.canonical_alias}/{message.event_id})\n"
                + f"Message matched phrase: `{phrasematch['phrase']}` classified as: `{phrasematch['reason']}`\n"
                + f"Below is the message source at the time of sending: \n ```\n{json.dumps(message.source, indent=3)}\n```",
            )


bot.run()
